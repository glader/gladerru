cd /app/src

echo 'yes' | python manage.py collectstatic

python manage.py migrate

gunicorn wsgi:application --workers 2 --bind :8000
