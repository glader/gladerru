FROM python:3.8

WORKDIR /app

COPY requirements.txt /app/requirements.txt
RUN pip install --upgrade pip && pip install -r /app/requirements.txt

COPY . .

CMD ./docker-entrypoint.sh

EXPOSE 8080
