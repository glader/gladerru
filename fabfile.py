import os
from copy import copy
from io import StringIO

from fabric import Connection
from fabric import task
from patchwork.files import exists
from fab_settings import *

connect_kwargs = {
    'passphrase': SSH_PASSPHRASE
}
c = Connection('188.246.227.206', user='root', connect_kwargs=connect_kwargs)

DIR = '/home/www/projects/gladerru'
ACTIVATE = 'source %s/ENV/bin/activate'


def init():
    with settings(user='root'):
        run('apt-get update -q', warn_only=True)
        #run('apt-get upgrade -y')
        #run('apt-get install -y mc nginx mysql-client libmysqlclient-dev python-setuptools python-dev python-pip rrdtool sendmail memcached fail2ban git')
        #run('apt-get build-dep -y python-mysqldb')
        #run('pip install --upgrade virtualenv')

        if not exists('/home/%s' % SSH_USER):
            run('yes | adduser --disabled-password %s' % SSH_USER)
            run('mkdir /home/%s/.ssh' % SSH_USER)
            run('echo "%s" >> /home/%s/.ssh/authorized_keys' % (env.www_ssh_key, SSH_USER))

        if not exists('/var/cache/gladerru/thumbnails'):
            run('mkdir -p /var/cache/gladerru/thumbnails')
            run('touch /var/cache/gladerru/glader_ru.links.db')
            run('chown -R www:www /var/cache/gladerru')

        if not exists('/var/log/projects/gladerru'):
            run('mkdir -p /var/log/projects/gladerru')
            run('chmod 777 /var/log/projects/gladerru')

        if exists('/etc/nginx/sites-enabled/default'):
            run('rm /etc/nginx/sites-enabled/default')

        if not exists('/etc/nginx/sites-available/gladerru.conf'):
            run('touch /etc/nginx/sites-available/gladerru.conf')
            run('chown %s /etc/nginx/sites-available/gladerru.conf' % SSH_USER)
        if not exists('/etc/nginx/sites-enabled/gladerru.conf'):
            run('ln -s /etc/nginx/sites-available/gladerru.conf /etc/nginx/sites-enabled/gladerru.conf', shell=False)

        if not exists('/etc/init/gladerru.conf'):
            run('touch /etc/init/gladerru.conf')
            run('chown %s /etc/init/gladerru.conf' % SSH_USER)

        if not exists('/etc/init/gladerru_celery.conf'):
            run('touch /etc/init/gladerru_celery.conf')
            run('chown %s /etc/init/gladerru_celery.conf' % SSH_USER)

        append('/etc/sudoers', '%s ALL=(ALL) NOPASSWD:/sbin/restart gladerru,/sbin/restart gladerru_celery' % SSH_USER)

        if not exists('/etc/cron.d/gladerru'):
            run('touch /etc/cron.d/gladerru')

        run('mkdir -p /home/%s/projects/gladerru' % SSH_USER)
        run('chown -R %(user)s:%(user)s /home/%(user)s' % {'user': SSH_USER})


def init_mysql():
    with settings(user='root'):
        run('apt-get update -q', warn_only=True)
        #run('apt-get upgrade -y')
        #run('apt-get install -y fail2ban mc')
        run('DEBIAN_FRONTEND=noninteractive apt-get -q -y install mysql-server')
        run('mysqladmin -u root password mysecretpasswordgoeshere111')

        #sed('/etc/mysql/my.cnf', 'bind-address.+$', 'bind-address = ::')
        run('/etc/init.d/mysql restart')


@task
def production(_):
    upload(_)
    static(_)
    environment(_)
    local_settings(_)
    nginx(_)
    systemd(_)
    cron(_)
    logrotate(_)
    #dump(_)
    migrate(_)
    collect_static(_)
    put_release_file(_)
    restart(_)


@task
def upload(_):
    c.local('git archive HEAD | gzip > archive.tar.gz')
    c.put('archive.tar.gz', DIR + '/archive.tar.gz')
    with c.cd(DIR):
        c.run('tar -zxf archive.tar.gz')
        c.run('rm archive.tar.gz')
    c.local('rm archive.tar.gz')


def virtualenv(command):
    with c.cd(DIR):
        c.run(ACTIVATE % DIR + ' && ' + command)


@task
def static(_):
    with c.cd(DIR + '/src/media/design/3/css'):
        c.run('python merge.py')


@task
def environment(_):
    with c.cd(DIR):
        c.run('mkdir -p ENV')
        c.run('python3 -m venv ENV')
        virtualenv('python3 -m pip install -r requirements.txt')


def upload_template(local_file, remote_file, context):
    with open(local_file) as inputfile:
        text = inputfile.read()

    if context:
        text = text % context

    return c.put(
        local=StringIO(text),
        remote=remote_file,
    )


@task
def local_settings(_):
    os.chdir(os.path.dirname(__file__))
    params = copy(globals())
    template_name = 'local_settings.py.sample'

    with c.cd(DIR):
        upload_template(
            'src/%s' % template_name,
            DIR + '/src/local_settings.py',
            params,
        )

        if exists(c, 'tools/dump/dump.sh'):
            upload_template(
                'tools/dump/dump.sh',
                DIR + '/tools/dump/dump.sh',
                params,
            )
            c.run('chmod +x tools/dump/dump.sh')


@task
def nginx(_):
    c.run('cp %s/tools/nginx/gladerru.conf /etc/nginx/sites-available/gladerru.conf' % DIR)
    c.run('/etc/init.d/nginx reload')


@task
def systemd(_):
    c.run('cp %s/tools/systemd/gladerru.service /lib/systemd/system/gladerru.service' % DIR)


@task
def cron(_):
    c.run('sed -i "s/\r//g" %s/tools/cron/gladerru' % DIR)
    c.run('cp %s/tools/cron/gladerru /etc/cron.d/gladerru' % DIR)
    c.run('systemctl restart cron')


@task
def logrotate(_):
    c.run('cp %s/tools/logrotate/gladerru /etc/logrotate.d/gladerru' % DIR)


@task
def dump(_):
    c.run('%s/tools/dump/dump.sh' % DIR)


def manage_py(command):
    virtualenv('cd %s && python3 src/manage.py %s' % (DIR, command))


@task
def migrate(_):
    manage_py('migrate')


@task
def collect_static(_):
    c.run('mkdir -p %s/static' % DIR)
    manage_py('collectstatic -c --noinput')


@task
def put_release_file(_):
    c.local('git log -n 1 --format=oneline >> release')

    c.put('release', DIR + '/release')
    c.local('rm release')


@task
def restart(_):
    c.run('systemctl daemon-reload')
    c.run('systemctl restart gladerru')


def remote(args=''):
    manage_py(args)


# -----------------------------------------------------------------------
# https://yandextank.readthedocs.org/en/latest/install.html

def tank_init():
    with settings(user='root', host='146.185.136.227'):
        run('apt-get install python-software-properties')
        run('add-apt-repository ppa:yandex-load/main')
        run('run apt-get update && sudo apt-get install yandex-load-tank-base')


def tank_start():
    with settings(user='root', host='146.185.136.227'):
        run('yandex-tank ammo.txt')


# -----------------------------------------------------------------------

def run_local():
    local('cd src && ..\\ENV\\Scripts\\python manage.py runserver 0.0.0.0:8000')


def local_env():
    with settings(warn_only=True):
        local('virtualenv.exe ENV --system-site-packages')
    local('ENV\\Scripts\\pip install -r requirements_test.txt ')


def enter(args=''):
    local('cd src && ..\\ENV\\Scripts\\python manage.py %s' % args)


def local_migrate():
    with settings(warn_only=True):
        local('cd src && ..\\ENV\\Scripts\\python manage.py makemigrations')
    local('cd src && ..\\ENV\\Scripts\\python manage.py migrate')


def update_local_db():
    run('mysqldump -u %(DATABASE_USER)s -p%(DATABASE_PASSWORD)s -h %(DATABASE_HOST)s %(DATABASE_DB)s |gzip > gladerru.sql.gz' % globals())
    get('gladerru.sql.gz', 'gladerru.sql.gz')
    run('rm gladerru.sql.gz')
    local('gzip -d gladerru.sql.gz')
    local('mysql -uroot %(DATABASE_DB)s < gladerru.sql' % globals())
    local('del gladerru.sql')


def local_celery():
    local('cd src && ..\\ENV\\scripts\\python manage.py celeryd --settings=settings')


def local_static():
    local('cd src && ..\\ENV\\scripts\\python manage.py collectstatic -c --noinput --verbosity=0')


def make_backup():
    today = date.today().replace(day=1)
    run('mysqldump -u %(DATABASE_USER)s -p%(DATABASE_PASSWORD)s -h %(DATABASE_HOST)s %(DATABASE_DB)s | gzip > gladerru.sql.gz' % globals())
    get('gladerru.sql.gz', 'gladerru.sql.%s.gz' % today.strftime('%Y%m%d'))
    run('rm gladerru.sql.gz')
